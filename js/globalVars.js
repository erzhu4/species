//animal specs that were not given in the data, will just hardcode them globally

window.GestationPeriod = {
	// numbers given by Sandra
	// 'kangaroo': 9,
	// 'bear' : 12

	// Decreased gestation period by me to give the species more of a fighting chance
	'kangaroo': 4,
	'bear' : 8
};

window.MinimumBreedingAge = {
	'kangaroo': 5,
	'bear' : 10
};

//lets start the year at spring to give the first few animals a fighting chance to survive
window.MonthToSeason = {
	0: 'spring',
	1: 'spring',
	2: 'spring',
	3: 'summer',
	4: 'summer',
	5: 'summer',
	6: 'fall',
	7: 'fall',
	8: 'fall',
	9: 'winter',
	10: 'winter',
	11: 'winter'
};

window.Genders = ['m', 'f'];

//generate a random number for temperature
window.generateRandomNumberBetween = function(low, high){
	return low + ((high - low) * Math.random());
}

window.averageNumbersInArray = function(arr){
	thisTotal = 0;
	for(var i = 0; i < arr.length; i++){
		thisTotal += parseInt(arr[i]);
	}
	// calculate average
	 return (thisTotal/arr.length);
}