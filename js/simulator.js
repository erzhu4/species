class Simulator {

	constructor(species, habitats, years, num){
		this.species = species;
		this.habitats = habitats;
		this.years = years;
		this.num = num;
	}

	simulate(){
		var results = {};

		this.species.forEach(specie => {
			var calculator = new SpeciesCalculator(specie, this.habitats, this.years);
			results[specie.name] = calculator.evaluate();
		});

		console.log(results);
	    return results;
	}

}