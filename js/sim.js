(function(){

  function parseYaml(content){
    return YAML.parse(content);
  }

  function paintThePageWithResults(){
    var resultsContainer = $("#results-container");
    var results = window.finalResult;

    resultsContainer.html("");

    resultsContainer.append('<h2>Your Results</h2>');

    Object.keys(results).forEach(specie => {

      resultsContainer.append('<h3>' + specie + '</h3>');

      Object.keys(results[specie]).forEach(habitat => {
        var data = results[specie][habitat];

        resultsContainer.append('<b>' + habitat + '</b><br>')
        resultsContainer.append(' - Average Population: ' + data.average_population + '<br>');
        resultsContainer.append(' - Max Population: ' + data.max_population + '<br>');
        resultsContainer.append(' - Mortality Rate: ' + data.mortality_rate + '%<br>');
      });

    });

  }


  function simulate(){
    if (!window.configData) {
      alert('please load in a file!');
      return;
    }

    var years = configData.years;
    var species = configData.species;
    var habitats = configData.habitats;

    var simulator = new Simulator(species, habitats, years, 1);
    

    window.finalResult = simulator.simulate();
    paintThePageWithResults();


  }


  function handleFile(event){
    var reader = new FileReader();

    reader.onload = function(event) {
      var contents = event.target.result;
      window.configData = parseYaml(contents);
    };

    var files = document.getElementById('file').files;

    reader.readAsText(files[0]);

  }


	$("#file").on('change', handleFile);
  $("#run-button").on('click', simulate);

})();