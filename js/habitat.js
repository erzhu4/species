class Habitat {

	constructor(params, specie){
		this.name = params['name'];
		this.specie = specie;
		this.average_temperature = params['average_temperature'];
		this.initial_food = params['monthly_food'];
		this.initial_water = params['monthly_water'];
		this.food = params['monthly_food'];
		this.water = params['monthly_water'];

		//start the habitat with a good survivable tempurature
		this.tempurature = 60;

		this.naturalDeaths = 0;
		this.unaturalDeaths = 0;

		this.animals = [new Animal(specie, 'm', this), new Animal(specie, 'f', this)];
	}

	animals(){
		return this.animals;
	}

	getPopulation(){
		return this.animals.length;
	}


	subtractFood(amount){
		this.food -=  amount;
	}

	subtractWater(amount){
		this.water -= amount;
	}

	incrementNaturalDeath(){
		this.naturalDeaths += 1;
	}

	incrementUnaturalDeath(){
		this.unaturalDeaths += 1;
	}

	removeAnimal(animal){
		this.animals.splice(this.animals.indexOf(animal), 1);
	}

	getTempurature(month){
		var average = this.average_temperature[MonthToSeason[month]]

		//0.5% of fluctuating 15 degrees
		if (Math.random() < 0.0005){
			return generateRandomNumberBetween( average - 15, average + 15 );
		} else {
			return generateRandomNumberBetween( average - 5, average + 5 );
		}
	}

	createNewAnimal(){
		this.animals.push(
			new Animal(
					this.specie,
					Genders[Math.floor(Math.random() * Genders.length)],
					this
				)
			);
	}

	resetMonth(month){
		this.food = this.initial_food;
		this.water = this.initial_water;
		this.tempurature = this.getTempurature(month);
	}

	iterateOneMonth(month){
		this.resetMonth(month);
		this.animals.forEach(animal => {
			animal.liveOneMonth();
		});
	}
}