class Animal {

	constructor(params, gender, habitat){
		this.name = params['name'];
		this.life_span = params['attributes']['life_span'];

		this.monthly_food_consumption = params['attributes']['monthly_food_consumption'];
		this.monthly_water_consumption = params['attributes']['monthly_water_consumption'];

		this.maximum_temperature = params['attributes']['maximum_temperature'];
		this.minimum_temperature = params['attributes']['minimum_temperature'];

		//default to 10 if we don't know
		this.gestationPeriod = GestationPeriod[this.name] ? GestationPeriod[this.name] : 10;
		this.minBreedingAge = MinimumBreedingAge[this.name] ? MinimumBreedingAge[this.name] : 10;

		this.gender = gender;
		this.alive = true;
		this.pregnant = false;
		//start closer to breeding age
		this.age_in_months = 150;
		this.habitat = habitat;

		this.months_without_food = 0;
		this.months_without_water = 0;
		this.months_out_of_tempurature = 0;
		this.months_pregnant = 0;
 
	}

	age(){
		return this.age_in_months / 12;
	}

	isOutSideOfTemperature(tempurature){
		return tempurature > this.maximum_temperature || tempurature < this.minimum_temperature;
	}

	//animal died of old age
	die(){
		this.alive = false;
		this.habitat.removeAnimal(this);
		this.habitat.incrementNaturalDeath();
	}

	//animal died of un-natural causes
	unaturallyDie(){
		this.alive = false;
		this.habitat.removeAnimal(this);
		this.habitat.incrementUnaturalDeath();
	}

	eatFood(){
		this.habitat.subtractFood(this.monthly_food_consumption);
	}

	drinkWater(){
		this.habitat.subtractWater(this.monthly_water_consumption);
	}

	giveBirth(){
		this.habitat.createNewAnimal();
		this.pregnant = false;
		this.months_pregnant = 0;
	}

	updateLivingConditions(){
		this.age_in_months += 1;

		if (this.age() >= this.life_span){
			this.die();
			return;
		}

		if (this.months_without_food >= 3){
			this.unaturallyDie();
			return;
		}

		if (this.months_without_water >= 1){
			this.unaturallyDie();
			return;
		}

		if (this.months_out_of_tempurature > 1){
			this.unaturallyDie();
			return;
		}

		if (this.habitat.food < 0){
			this.months_without_food += 1;
		} else {
			this.months_without_food = 0;
		}

		if (this.habitat.water < 0){
			this.months_without_water += 1;
		} else {
			this.months_without_water = 0;
		}

		if (this.isOutSideOfTemperature(this.habitat.tempurature)){
			this.months_out_of_tempurature += 1;
		} else {
			this.months_out_of_tempurature = 0;
		}
	}

	updateBreedingConditions(){
		if (this.gender != 'f'){
			return;
		}

		//is currently pregnant
		if (this.pregnant) {
			this.months_pregnant += 1;
			if (this.months_pregnant >= this.gestationPeriod){
				this.giveBirth();
				return;
			}
			return;
		} else {
			//not pregnant
			if (this.age() < this.minBreedingAge) {
				return;
			} else {
				this.pregnant = true;
			}
		}

	}

	liveOneMonth(){
		if (!this.alive){
			return;
		}

		this.updateLivingConditions();
		this.updateBreedingConditions();

		//did I die this month?
		if (!this.alive){
			return;
		}

		this.eatFood();
		this.drinkWater();
	}

}