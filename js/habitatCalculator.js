class HabitatCalculator {

	constructor(specie, habitat, years){
		this.specie = specie;
		this.habitat = new Habitat(habitat, specie);
		this.years = years;

		this.maxPopulation = 0;
		this.monthlyPopulation = [];
	}

	updateMaxPop(pop){
		if (pop > this.maxPopulation){
			this.maxPopulation = pop;
		}
	}

	evaluate(){
		for (var year = 0; year < this.years; year++){
			for (var month = 0; month < 12; month++){

				this.habitat.iterateOneMonth(month);

				var populationThisMonth = this.habitat.getPopulation();

				//only count the months where there are any animals alive
				if (populationThisMonth > 0){
					this.monthlyPopulation.push(populationThisMonth);
				}

				this.updateMaxPop(populationThisMonth);
			}
		}

		return {
			'average_population': averageNumbersInArray(this.monthlyPopulation),
			'max_population': this.maxPopulation,
			'mortality_rate': this.habitat.unaturalDeaths / (this.habitat.unaturalDeaths + this.habitat.naturalDeaths +this.habitat.getPopulation()) * 100
		};
	}

}