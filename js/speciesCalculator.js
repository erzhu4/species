class SpeciesCalculator {

	constructor(specie, habitats, years){
		this.specie = specie;
		this.habitats = habitats;
		this.years = years;
	}

	evaluate(){
		var results = {};

		this.habitats.forEach(habitat => {
			var habitatCalculator = new HabitatCalculator(this.specie, habitat, this.years);
			results[habitat.name] = habitatCalculator.evaluate();
		});
		return results;
	}

}